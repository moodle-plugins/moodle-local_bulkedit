<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkedit\forms;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . '/formslib.php');

class confirm_form extends \moodleform {
    protected $data;
    protected $selectedcms;
    protected $courseid;

    public function __construct($data, $selectedcms, $courseid, $action = null) {
        $this->metadata = (array) $data;
        $this->selectedcms = $selectedcms;
        $this->courseid = $courseid;
        parent::__construct($action);
    }

    protected function definition() {
        global $OUTPUT, $DB;
        $mform = $this->_form;

        $mform->addElement('hidden', 'step', 4);
        $mform->setType('step', PARAM_INT);

        $mform->addElement('hidden', 'courseid', $this->courseid);
        $mform->setType('courseid', PARAM_INT);

        $mform->addElement('hidden', 'data', json_encode($this->data));
        $mform->setType('data', PARAM_RAW);

        $mform->addElement('hidden', 'selectedcms', json_encode($this->selectedcms));
        $mform->setType('selectedcms', PARAM_RAW);

        $mform->addElement('html', '<div class="mb-2">' . get_string('metadatachangespreviewmessage', 'metadatacontext_module') . '</div>');

        $table = new \html_table();
        $table->head = array(
                get_string('modulename', 'metadatacontext_module')
        );
        $dataoutput = new output\manage_data();
        foreach ($dataoutput->data as $items) {
            unset($items['categoryname']);

            foreach ($items as $item) {
                if (isset($this->metadata[$item->inputname])) {
                    $table->head[] = format_string($item->field->name);
                }
            }
        }

        $cms = get_fast_modinfo($this->courseid, -1)->cms;
        foreach ($this->selectedcms as $cmid) {
            if (isset($cms[$cmid])) {
                $cm = $cms[$cmid];
                $icon = $OUTPUT->image_icon('icon', $cm->get_module_type_name(), $cm->modname, array('class' => 'activityicon'));
                $row = array('<span class="activity">' . $icon . $cm->get_formatted_name() . '</span>');

                $dataoutput = new output\manage_data($cm);
                foreach ($dataoutput->data as $items) {
                    unset($items['categoryname']);
                    /**
                     * @var \local_metadata\fieldtype\metadata $item
                     */
                    foreach ($items as $item) {
                        if (isset($this->metadata[$item->inputname])) {
                            $oldvalue = $DB->get_field('local_metadata', 'data', array('fieldid' => $item->field->id, 'instanceid' => $cmid));
                            $oldvaluedisplay = $item->display_data();
                            $newvalue = $this->metadata[$item->inputname];
                            $item->data = $item->edit_save_data_preprocess(is_object($newvalue) ? (array) $newvalue : $newvalue, new \stdClass());
                            $newvaluedisplay = $item->display_data();
                            $info = '';
                            if ($oldvalue === false || $oldvalue === null) {
                                $info = '<b class="text-success mr-1">[' . get_string('new') . ']</b>';
                                $oldvaluedisplay = null;
                            } else if ($oldvaluedisplay == $newvaluedisplay) {
                                $info = '<b class="text-info mr-1">[' . get_string('unchanged', 'metadatacontext_module') . ']</b>';
                                $oldvaluedisplay = null;
                            } else {
                                $info = '<b class="text-warning mr-1">[' . get_string('overwrite', 'metadatacontext_module') . ']</b>';
                            }
                            if ($oldvaluedisplay === '') {
                                $oldvaluedisplay = '<span>[' . get_string('emptysettingvalue', 'admin') . ']</span>';
                            }
                            if ($newvaluedisplay === '') {
                                $newvaluedisplay = '<span>[' . get_string('emptysettingvalue', 'admin') . ']</span>';
                            }
                            $row[] = $info . ($oldvaluedisplay !== null ? $oldvaluedisplay . '<i class="fa fa-fw fa-caret-right"></i>' : '') . $newvaluedisplay;
                        }
                    }
                }
                $table->data[] = $row;
            }
        }

        $mform->addElement('html', \html_writer::table( $table ));

        $buttonarray = array();
        $buttonarray[] = &$mform->createElement('cancel', 'cancel', get_string('back'));
        $buttonarray[] = &$mform->createElement('submit', 'confirmbutton', get_string('confirm'));
        $mform->addGroup($buttonarray, 'buttonar', '', ' ', false);
        $mform->closeHeaderBefore('buttonar');
    }
}

<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkedit\forms;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . '/formslib.php');

class select_data_form extends \moodleform {
    protected $selectedcms;
    protected $courseid;

    public function __construct($selectedcms, $courseid, $action = null) {
        $this->selectedcms = $selectedcms;
        $this->courseid = $courseid;
        parent::__construct($action);
    }

    protected function definition() {
        global $OUTPUT;
        $mform = $this->_form;

        $mform->addElement('hidden', 'step', 2);
        $mform->setType('step', PARAM_INT);

        $mform->addElement('hidden', 'courseid', $this->courseid);
        $mform->setType('courseid', PARAM_INT);

        $mform->addElement('hidden', 'selectedcms', json_encode($this->selectedcms));
        $mform->setType('selectedcms', PARAM_RAW);

        $modinfo = get_fast_modinfo($this->courseid, -1);
        $cmsbytype = [];
        foreach ($this->selectedcms as $cmid) {
            $cminfo = $modinfo->get_cm($cmid);
            if (!isset($cmsbytype[$cminfo->modname])) {
                $cmsbytype[$cminfo->modname] = [];
            }
            $cmsbytype[$cminfo->modname][$cmid] = $cminfo->get_formatted_name();
        }

        $mform->addElement('header', 'selectedcmssummary', 'Selected modules');
        $mform->setExpanded('selectedcmssummary');

        foreach ($cmsbytype as $type => $cms) {
            $mform->addElement('static', null,
                    $OUTPUT->image_icon('icon', '', $type, [ 'class' => 'activityicon' ]) . get_string('modulenameplural', 'mod_' . $type),
                    implode('<br>', $cms));
        }

        if (count($cmsbytype) > 1) {
            $mform->addElement('static', null, null, 'You are about to edit data of modules of different types.<br>' .
                    'Please be aware that several module types can manage data with a same name in different ways.<br>' .
                    'Use this functionality at your own risk.');
        }

        $mform->addElement('header', 'selectdata', get_string('selectdata', 'local_bulkedit'));
        $mform->setExpanded('selectedcmssummary');

        $buttonarray = [];
        $buttonarray[] = &$mform->createElement('cancel', 'cancel', get_string('back'));
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('continue'));
        $mform->addGroup($buttonarray, 'buttonar', '', ' ', false);
        $mform->closeHeaderBefore('buttonar');

    }

    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
        return $errors;
    }
}

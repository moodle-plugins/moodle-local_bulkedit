<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkedit\forms;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . '/formslib.php');

class select_modules_form extends \moodleform {
    protected $courseid;

    public function __construct($courseid, $action = null) {
        $this->courseid = $courseid;
        parent::__construct($action);
    }

    protected function definition() {
        global $OUTPUT;
        $mform = $this->_form;

        $mform->addElement('hidden', 'step', 1);
        $mform->setType('step', PARAM_INT);

        $mform->addElement('hidden', 'courseid', $this->courseid);
        $mform->setType('courseid', PARAM_INT);

        $mform->addElement('html', \html_writer::tag('h3', get_string('selectmodules', 'local_bulkedit')));

        $cms = get_fast_modinfo($this->courseid, -1)->cms;

        if (count($cms)) {
            $sectionid = -1;
            foreach ($cms as $cm) {
                if ($cm->section != $sectionid) {
                    $sectionid = $cm->section;
                    $mform->addElement('html',
                            '<h3 class="headerwithcontroller">' . get_section_name($this->courseid, $cm->sectionnum) . '</h3>');
                    $this->add_checkbox_controller($sectionid);
                }
                $icon = $OUTPUT->image_icon('icon', $cm->get_module_type_name(), $cm->modname, [ 'class' => 'activityicon' ]);
                $mform->addElement('advcheckbox', 'cm_' . $cm->id, $icon . $cm->get_formatted_name(), '', [ 'group' => $sectionid, 'class' => 'my-1' ]);
            }
        }

        $buttonarray = [];
        $buttonarray[] = &$mform->createElement('cancel');
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('continue'));
        $mform->addGroup($buttonarray, 'buttonar', '', ' ', false);
        $mform->closeHeaderBefore('buttonar');
    }

    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
        $ok = false;
        foreach ($data as $field => $value) {
            if (substr($field, 0, 3) === 'cm_' && $value == 1) {
                $ok = true;
                break;
            }
        }
        if (!$ok) {
            $errors['buttonar'] = get_string('selectmodules_error', 'local_bulkedit');
        }
        return $errors;
    }
}

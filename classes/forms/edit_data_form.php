<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkedit\forms;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . '/formslib.php');

class edit_data_form extends \moodleform {
    protected $selecteddata;
    protected $selectedcms;
    protected $courseid;

    public function __construct($selecteddata, $selectedcms, $courseid, $action = null) {
        $this->selecteddata = $selecteddata;
        $this->selectedcms = $selectedcms;
        $this->courseid = $courseid;
        parent::__construct($action);
    }

    protected function definition() {
        global $OUTPUT;
        $mform = $this->_form;

        $mform->addElement('hidden', 'step', 3);
        $mform->setType('step', PARAM_INT);

        $mform->addElement('hidden', 'courseid', $this->courseid);
        $mform->setType('courseid', PARAM_INT);

        $mform->addElement('hidden', 'selecteddata', json_encode($this->selecteddata));
        $mform->setType('selecteddata', PARAM_RAW);

        $mform->addElement('hidden', 'selectedcms', json_encode($this->selectedcms));
        $mform->setType('selectedcms', PARAM_RAW);

        $dataoutput = new output\manage_data();

        $mform->addElement('header', 'headermetadata', get_string('fillinmetadata', 'metadatacontext_module'));
        foreach ($dataoutput->data as $catid => $items) {
            $mform->addElement('static', 'headercategory' . $catid, '', '<h3>' . $items['categoryname'] . '</h3>');
            unset($items['categoryname']);

            foreach ($items as $item) {
                if (in_array($item->inputname, $this->selectedmetadata)) {
                    $item->edit_field($mform);
                }
            }
        }

        $mform->addElement('header', 'modulesheader', get_string('selectmodules', 'metadatacontext_module'));

        $cms = get_fast_modinfo($this->courseid, -1)->cms;

        if (count($cms)) {
//             $this->add_checkbox_controller(1);

            $group = array();
            $sectionid = -1;
            foreach ($cms as $cm) {
                if ($cm->section != $sectionid) {
                    $sectionid = $cm->section;
                    $group[] =& $mform->createElement('static', 'headersection' . $sectionid, '', '<h3>' . get_section_name($this->courseid, $cm->sectionnum) . '</h3>');
                }
                $icon = $OUTPUT->image_icon('icon', $cm->get_module_type_name(), $cm->modname, array('class' => 'activityicon'));
                $group[] =& $mform->createElement('advcheckbox', 'applytocm' . $cm->id, $icon . $cm->get_formatted_name(), '', array('group' => 1, 'class' => 'my-1'));
            }
        }
        $mform->addGroup($group, 'groupmodules', get_string('selectmodules', 'metadatacontext_module'), '<br>', false);

        $buttonarray = array();
        $buttonarray[] = &$mform->createElement('cancel', 'cancel', get_string('back'));
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('continue'));
        $mform->addGroup($buttonarray, 'buttonar', '', ' ', false);
        $mform->closeHeaderBefore('buttonar');
    }

    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
        $ok = false;
        foreach ($data as $field => $value) {
            if (substr($field, 0, 9) == 'applytocm' && $value == 1) {
                $ok = true;
                break;
            }
        }
        if (!$ok) {
            $errors['groupmodules'] = get_string('selectmodules_error', 'metadatacontext_module');
        }
        return $errors;
    }
}

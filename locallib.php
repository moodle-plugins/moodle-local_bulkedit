<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die;

/**
 * Returns HTML with icon and name for a given course module.
 * @param cm_info|null $cminfo Course module info.
 * @param boolean $withlink Whether to create link to activity view page.
 * @return string HTML fragment.
 */
function local_bulkedit_activity_icon_and_name($cminfo, $withlink = true) {
    $icon = '<img class="activityicon" src="' . $cminfo->get_icon_url()->out() . '"/>';
    $name = '<span class="activity">' . $icon . $cminfo->get_formatted_name() . '</span>';
    if ($withlink && $cminfo->url !== null) {
        $name = '<a href="' . $cminfo->url->out() . '">' . $name . '</a>';
    }
    return $name;
}

function local_bulkedit_print_progress_and_content($content, $currentstep) {
    global $OUTPUT;
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('bulkedit', 'local_bulkedit'));
    if ($currentstep !== null) {

        $steps = [ 'selectmodules', 'selectdata', 'editdata', 'confirmandfinish' ];
        $stepdots = [];

        $dotsize = 10;

        foreach ($steps as $step => $steptitle) {
            $html = '<div style="width:' . $dotsize . 'px;display:grid;justify-content:center;">';
            $html .= '<div class="border border-dark rounded-circle' . ($currentstep >= $step ? ' bg-primary' : '') . '" style="justify-self:center;width:' . $dotsize . 'px;height:' . $dotsize . 'px;"></div>';
            $html .= '<div class="text-center" style="user-select:none;width:8em;font-weight:' . ($currentstep === $step ? '600' : '400') . '">' . get_string($steptitle, 'local_bulkedit') . '</div>';
            $html .= '</div>';
            $stepdots[] = $html;
        }

        $line = '<div class="border border-dark" style="margin-top: ' . ($dotsize/2 - 1) . 'px; flex-basis:20%; min-width: 8em; height:1px;"></div>';

        echo \html_writer::div(implode($line, $stepdots), 'd-flex justify-content-center align-items-start m-y-1');
    }
    echo $content;
    echo $OUTPUT->footer();
}
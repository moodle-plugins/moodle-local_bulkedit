<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die;

function local_bulkedit_extend_navigation_course($navigation, $course, $context) {
    global $PAGE;

    if (!$PAGE->course or $PAGE->course->id == 1) {
        return;
    }

    if (has_capability('moodle/course:update', $context)) {
        $url = new moodle_url('/local/bulkedit/index.php', array('courseid' => $course->id));
        $name = get_string('bulkedit', 'local_bulkedit');
        $navigation->add($name, $url, navigation_node::TYPE_SETTING, null, null, new pix_icon('bulkedit', '', 'local_bulkedit'));
    }
}
/**
 * Definition of Fontawesome icons mapping.
 * @return string[] Fontawesome icons mapping.
 */
function local_bulkedit_get_fontawesome_icon_map() {
    return [
            'local_bulkedit:bulkedit' => 'fa-list'
    ];
}
<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/locallib.php');

use local_bulkedit\forms\select_modules_form;
use local_bulkedit\forms\select_data_form;
use local_bulkedit\forms\edit_data_form;
use local_bulkedit\forms\confirm_form;

global $DB, $PAGE, $OUTPUT;

$courseid = required_param('courseid', PARAM_INT);
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = context_course::instance($courseid);

require_login();
require_capability('moodle/course:update', $context);

$PAGE->set_course( $course );
$PAGE->set_context( $context );
$PAGE->set_title( $course->fullname . ' - ' . get_string( 'bulkedit', 'local_bulkedit' ) );
$PAGE->set_pagelayout( 'incourse' );
$PAGE->set_heading( $course->fullname );
$PAGE->set_url( '/local/bulkedit/index.php', array( 'id' => $courseid ) );

$courseurl = course_get_url($courseid);

// echo $OUTPUT->header();
// echo $OUTPUT->heading( get_string( 'bulkedit', 'local_bulkedit' ) );
// echo $OUTPUT->box_start();


$step0data = null;
$step1data = null;
$step2data = null;

$step = optional_param('step', 0, PARAM_INT);

echo '<br><br><br><br><br>';
print_r($step);

if ($step == 4 && !empty($_POST['data']) && !empty($_POST['selectedcms'])) {
    $data = json_decode($_POST['data']);
    $selectedcms = json_decode($_POST['selectedcms']);
    $confirmform = new confirm_form($data, $selectedcms, $courseid, $PAGE->url);
    if ($confirmform->get_data() !== null) {
        confirm_sesskey();

        // Everything is fine, save the submitted data.
        $data = (object) $data;
        // TODO
//         require_once(__DIR__ . '/../../lib.php');
//         foreach ($cmids as $cmid) {
//             $metadata->id = $cmid;
//             if (has_capability('moodle/course:manageactivities', \context_module::instance($cmid))) {
//                 \local_metadata_save_data($metadata, CONTEXT_MODULE);
//             }
//         }

        $content = $OUTPUT->notification(get_string('success'), \core\output\notification::NOTIFY_SUCCESS);
        $content .= $OUTPUT->continue_button($courseurl);
        local_bulkedit_print_progress_and_content($content, null);
        die();
    } else {
        // The "Back" button has been pressed.
        // Go back to the previous step with previously filled values.
        $step = 2;
        $step2data = array();
        foreach ($data as $field => $value) {
            $step2data['data_' . $field] = $value;
            $step2data['data_field_' . $field] = 1;
        }
        foreach ($selectedcms as $cmid) {
            $step2data['cm_' . $cmid] = 1;
        }
    }
}

if ($step == 3 && !empty($_POST['selectedcms']) && !empty($_POST['selecteddata'])) {
    $selectedcms = json_decode($_POST['selectedcms']);
    $selecteddata = json_decode($_POST['selecteddata']);
    $editform = new edit_data_form($selectedcms, $selecteddata, $courseid, $PAGE->url);
    if (($formdata = $editform->get_data()) !== null) {
        $data = array();
        foreach ($formdata as $field => $value) {
            if (substr($field, 0, 5) == 'data_') {
                $data[substr($field, 5)] = $value;
            }
        }
        $confirmform = new confirm_form($data, $selectedcms, $courseid, $PAGE->url);
        local_metadata_print_progress_and_content($confirmform->render(), 2);
        die();
    } else if ($editform->is_cancelled()) {
        $step = 1;
        $step1data = array();
        foreach ($selecteddata as $field) {
            $step1data['data_field_' . $field] = 1;
        }
        foreach ($selectedcms as $cmid) {
            $step1data['cm_' . $cmid] = 1;
        }
    } else {
        local_metadata_print_progress_and_content($editform->render(), 1);
        die();
    }
}

if ($step == 2 && !empty($_POST['selectedcms'])) {
    $selectedcms = json_decode($_POST['selectedcms']);
    $selectdataform = new select_data_form($selectedcms, $courseid, $PAGE->url);
    if (($formdata = $selectdataform->get_data()) !== null || $step2data !== null) {
        if ($formdata === null) {
            $formdata = $step2data;
        }
        $selecteddata = array();
        foreach ($formdata as $field => $value) {
            if (substr($field, 0, 11) == 'data_field_' && $value == 1) {
                $selecteddata[] = substr($field, 11);
            }
        }
        $editdataform = new edit_data_form($selecteddata, $selectedcms, $courseid, $PAGE->url);
        $editdataform->set_data($step2data);
        local_bulkedit_print_progress_and_content($editdataform->render(), 2);
        die();
    } else if ($selectdataform->is_cancelled()) {
        $step = 0;
        $step0data = array();
        foreach ($selectedcms as $cmid) {
            $step0data['cm_' . $cmid] = 1;
        }
    } else {
        local_bulkedit_print_progress_and_content($selectdataform->render(), 1);
        die();
    }
}

if ($step == 0 || $step == 1) {
    $selectcmsform = new select_modules_form($courseid, $PAGE->url);
    if (($formdata = $selectcmsform->get_data()) !== null || $step1data !== null) {
        if ($formdata === null) {
            $formdata = $step1data;
        }
        $selectedcms = array();
        foreach ($formdata as $field => $value) {
            if (substr($field, 0, 3) == 'cm_' && $value == 1) {
                $selectedcms[] = substr($field, 3);
            }
        }

        $selectdataform = new select_data_form($selectedcms, $courseid, $PAGE->url);
        $selectdataform->set_data($step1data);
        local_bulkedit_print_progress_and_content($selectdataform->render(), 1);
        die();
    } else if ($selectcmsform->is_cancelled()) {
        redirect($courseurl);
        die();
    } else {
        $selectcmsform->set_data($step0data);
        local_bulkedit_print_progress_and_content($selectcmsform->render(), 0);
        die();
    }
}

// We are in an inconsistent state.
redirect($courseurl);
die();


// $step = optional_param('step', 0, PARAM_INT);

// print_r($step);

// if ($step === 0 || $step === 1) {
//     $form = new select_modules_form($courseid, $PAGE->url);
//     if ($form->is_cancelled()) {
//         redirect(course_get_url($courseid));
//     } else if (($data = $form->get_data()) !== null) {
//         $step = 1;
//         $selectedcms = [];
//         $form = new select_data_form($data, $courseid);
//     }
// } else if ($step === 1) {
//     $form = new select_data_form();
//     if ($form->is_cancelled()) {
//         $step = 0;
//         $form = new select_modules_form($courseid, $PAGE->url);
//     } else if (($data = $form->get_data()) !== null) {
//         $step = 2;
//         $form = null;
//     }
// }
// local_bulkedit_print_progress_and_content($form->render(), $step);
